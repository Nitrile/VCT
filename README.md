# Valconsa Code Transformer, a front-end of the Valconsa language

## Building and Installation

To build uop,
```bash
mkdir bin && mkdir bin/build && cd bin/build
cmake ../.. -DCMAKE_BUILD_TYPE=Release
cmake --build .
```

The `uopConfig.cmake` file will be located in `bin/`. Simply appending this
directory to `CMAKE_PREFIX_PATH` allows `find_package(uop)` to find it. Since
`uop` is a lightweight static library, it can be flexibly linked into different
executables.

## Development and Testing

Building uop in a debug environment requires slightly different flags.
```bash
mkdir bin && mkdir bin/build && cd bin/build
cmake ../.. -DCMAKE_BUILD_TYPE=Debug -DTEST=1
cmake --build .
```
To execute tests:
```bash
cd bin/build
ctest
```

