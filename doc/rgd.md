# VCT.rgd, A dependency graph resolver

## Introduction

The RGD is designed to handle the interdependency of Valconsa's node tree to
allow arbitrarily complex dependency problems at compile time.

* A "node" is a list of alphanumeric identifiers. e.g. `std.lang.string`. In
  Valconsa, a node is a namespace, class, function, type, or value.
* A "branch" is an aggregate of identifiers. e.g. A file `std/container.v`
  containing some nodes. All nodes in a branch must be children of the branch
  node. e.g. All nodes in `std/container.v` must be prefixed by
  `std.container`. A branch almost always corresponds to a disk file.
* A "tree" is a branch located in its own directory, an analogue to a
  "package".

### Polyphasic Compilation

If the branches in a source tree form a directed depdency graph, each branch
only needs to be processed through the compiler once. This is the trivial case.
The catch to RGD's complexity happens when resolving circular dependencies.
