# VCT.lex, A robust lexer

## Application

VCT.lex can be used with a language satisfying the following conditions:

* A contiguous block of `A-Za-z_` followed by `A-Za-z0-9_` is a
  "alphanumeric identifier".
* Delimiters `()[]{}` always occur in pairs (allows multilingual parsing)
* A contiguous block of symbols is a "symbolic identifier". One consequence is
  that `;;;` is not lexed as 3 separate `;`'s and space is mandatory: `; ; ;`.
* A contiguous block starting with `0-9` is a numeric literal.
* A contiguous block delimited by `'` or `"` is a character literal/string
  literal.
* ` `, `\t`, `\n` are ignored except for separating tokens.

Example:
```
def f(int x)
{
	out.print("Hello World! " + x);
}
```

## Tokens and Lexer

The `Token` class (`lex/Token.hpp`) consists of a category `category()` field
and a content `content()` field, representing the captured characters.

To add more categories to the `Token` class, create a `enum` using
`Token::Category` as a base type:
```cpp
enum CategoryLang: Token::Category
{
	K_CLASS = Token::CAT_MAX,
	K_DEFINE,
	K_CONSTANT,
	...
};
```
and create two categoriser functions, one corresponding to alphanumeric
identifier (`class`, `const`), and one to symbolic identifier (`:=`, `->`). 
```cpp
Token::Category
categoriserAlpha(char const* const str)
{
	if (!strcmp(str, "class"))
		return K_CLASS;
	if (!strcmp(str, "def"))
		return K_DEFINE;
	...
	return I_ALPHA;
}
Token::Category
categoriserSymbol(char const* const str)
{
	...
}
```

Now, create a lexer from a input stream and couple the categorisers to it:
```cpp
ITokenStream its(&std::cin);
its.setCategoriserAlpha(&categoriserAlpha);
its.setCategoriserSymbol(&categoriserSymbol);
```
Each token is lexed with
```cpp
Token t;
its >> t;
```
