# Branching Model

The following branches are for core developments:

* `master`: Main development branch
* `release`: Release branch

Branches that correspond to individual libraries have the same name as the
libraries. Branches of features in a library are of the format
`[library].[feature]`.

* `lex`: Lexing library
* `sa`: Syntactical analysis (parsing) library
* `dr`: Tree dependency resolver
