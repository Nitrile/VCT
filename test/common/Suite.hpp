#ifndef __VCT_test_common_Suite_hpp_
#define __VCT_test_common_Suite_hpp_

#include <iostream>

class Suite
{
public:
	Suite(): total_(0) {}

	void assert(bool flag, char const* msg)
	{
		if (!flag)
		{
			++total_;
			std::cerr << "Fail: " << msg << '\n';
		}
	}
	void assert(bool flag, std::string const& msg)
	{
		assert(flag, msg.c_str());
	}

	template <typename T0, typename T1> void
	assertEqual(T0 const& x0, T1 const& x1, char const* msg = "")
	{
		bool const flag = x0 == x1;
		if (!flag)
		{
			++total_;
			std::cerr << "Unequal: " << x0 << " != " << x1 << ": " << msg << "\n";
		}
	}

	/**
	 * Return this value at the end of main function
	 */
	int accumulate() const { return !!total_; }
private:
	int total_;
};

#endif // !__VCT_test_common_Suite_hpp_
