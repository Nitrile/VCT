#include <common/Suite.hpp>

#include <sstream>

#include <VCT/lex/ITokenStream.hpp>

#include "categoriser.hpp"

void case_1(Suite& suite)
{
	using namespace vct::lex;

	std::istringstream iss("def f() = {\"test\"}");

	ITokenStream its(&iss);

	Token t;
	its >> t;
	suite.assertEqual(t.category(), Token::I_ALPHA, "Cat 1");
	suite.assertEqual(t.content(), "def", "Content 1");
	its >> t;
	suite.assertEqual(t.category(), Token::I_ALPHA, "Cat 2");
	suite.assertEqual(t.content(), "f", "Content 2");
	its >> t;
	suite.assertEqual((int) t.category(), (int) Token::D_BRACKET_L, "Cat 3");
	its >> t;
	suite.assertEqual((int) t.category(), (int) Token::D_BRACKET_R, "Cat 4");
	its >> t;
	suite.assertEqual(t.category(), Token::I_SYMB, "Cat 5");
	suite.assertEqual(t.content(), "=", "Content 5");
	its >> t;
	suite.assertEqual(t.category(), Token::D_BRACKET_C_L, "Cat 6");
	its >> t;
	suite.assertEqual(t.category(), Token::L_STRING, "Cat 7");
	suite.assertEqual(t.content(), "test", "Content 7");
	its >> t;
	suite.assertEqual(t.category(), Token::D_BRACKET_C_R, "Cat 8");
	its >> t;
	suite.assertEqual(t.category(), Token::END, "Cat 9");
}
void case_position(Suite& suite)
{
	char const* const data = 
		"old_macdonald.had(a form)\n"
		"{\n"
		  "\te_i ^^ e_i = 0;\n"
		"};\n";
	
	using namespace vct::lex;

	std::istringstream iss(data);
	ITokenStream its(&iss);

	Token t;
	its >> t;
	suite.assertEqual(t.content(), "old_macdonald", "old_macdonald:S");
	suite.assertEqual(t.category(), Token::I_ALPHA, "old_macdonald:C");
	suite.assertEqual(t.row(), 1, "old_macdonald:Y");
	suite.assertEqual(t.col(), 1, "old_macdonald:X");

	its >> t;
	suite.assertEqual(t.content(), ".", ".:S");
	suite.assertEqual(t.category(), Token::I_SYMB, ".:C");
	suite.assertEqual(t.row(), 1, ".:Y");
	suite.assertEqual(t.col(), 14, ".:X");

	its >> t;
	suite.assertEqual(t.content(), "had", "had:S");
	suite.assertEqual(t.category(), Token::I_ALPHA, "had:C");
	suite.assertEqual(t.row(), 1, "had:Y");
	suite.assertEqual(t.col(), 15, "had:X");

	its >> t;
	suite.assertEqual(t.category(), Token::D_BRACKET_L, "(:C");
	suite.assertEqual(t.row(), 1, "(:Y");
	suite.assertEqual(t.col(), 18, "(:X");

	its >> t;
	suite.assertEqual(t.content(), "a", "a:S");
	suite.assertEqual(t.category(), Token::I_ALPHA, "a:C");
	suite.assertEqual(t.row(), 1, "a:Y");
	suite.assertEqual(t.col(), 19, "a:X");

	its >> t;
	suite.assertEqual(t.content(), "form", "form:S");
	suite.assertEqual(t.category(), Token::I_ALPHA, "form:C");
	suite.assertEqual(t.row(), 1, "form:Y");
	suite.assertEqual(t.col(), 21, "form:X");

	its >> t;
	suite.assertEqual(t.category(), Token::D_BRACKET_R, "):C");
	suite.assertEqual(t.row(), 1, "):Y");
	suite.assertEqual(t.col(), 25, "):X");

	its >> t;
	suite.assertEqual(t.category(), Token::D_BRACKET_C_L, "{:C");
	suite.assertEqual(t.row(), 2, "{:Y");
	suite.assertEqual(t.col(), 1, "{:X");

	its >> t;
	suite.assertEqual(t.content(), "e_i", "e_i:S");
	suite.assertEqual(t.category(), Token::I_ALPHA, "e_i:C");
	suite.assertEqual(t.row(), 3, "e_i:Y");
	suite.assertEqual(t.col(), 2, "e_i:X");

	its >> t;
	suite.assertEqual(t.content(), "^^", "^^:S");
	suite.assertEqual(t.category(), Token::I_SYMB, "^^:C");
	suite.assertEqual(t.row(), 3, "^^:Y");
	suite.assertEqual(t.col(), 6, "^^:X");

	its >> t;
	suite.assertEqual(t.content(), "e_i", "e_i:S");
	suite.assertEqual(t.category(), Token::I_ALPHA, "e_i:C");
	suite.assertEqual(t.row(), 3, "e_i:Y");
	suite.assertEqual(t.col(), 9, "e_i:X");

	its >> t;
	suite.assertEqual(t.content(), "=", "=:S");
	suite.assertEqual(t.category(), Token::I_SYMB, "=:C");
	suite.assertEqual(t.row(), 3, "=:Y");
	suite.assertEqual(t.col(), 13, "=:X");

	its >> t;
	suite.assertEqual(t.content(), "0", "0:S");
	suite.assertEqual(t.category(), Token::L_INT, "0:C");
	suite.assertEqual(t.row(), 3, "0:Y");
	suite.assertEqual(t.col(), 15, "0:X");

	its >> t;
	suite.assertEqual(t.content(), ";", ";:S");
	suite.assertEqual(t.category(), Token::I_SYMB, ";:C");
	suite.assertEqual(t.row(), 3, ";:Y");
	suite.assertEqual(t.col(), 16, ";:X");

	its >> t;
	suite.assertEqual(t.category(), Token::D_BRACKET_C_R, "}:C");
	suite.assertEqual(t.row(), 4, "}:Y");
	suite.assertEqual(t.col(), 1, "}:X");

	its >> t;
	suite.assertEqual(t.content(), ";", ";:S");
	suite.assertEqual(t.category(), Token::I_SYMB, ";:C");
	suite.assertEqual(t.row(), 4, ";:Y");
	suite.assertEqual(t.col(), 2, ";:X");

	its >> t;
	suite.assertEqual(t.category(), Token::END, "EOF:C");
	suite.assertEqual(t.row(), 5, "EOF:Y");
	suite.assertEqual(t.col(), 1, "EOF:X");
}

int main()
{
	Suite suite;

	case_1(suite);
	case_position(suite);
	case_categoriser(suite);

	return suite.accumulate();
}
