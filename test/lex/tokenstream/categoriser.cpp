#include "categoriser.hpp"

#include <common/Suite.hpp>
#include <VCT/lex/ITokenStream.hpp>

#include <cstring>
#include <sstream>

using namespace vct::lex;

namespace
{

enum Category2: Token::Category
{
	K_DEF = Token::CAT_MAX,
};

Token::Category
categoriserAlpha(char const* const c)
{
	if (!strcmp(c, "def"))
		return K_DEF;
	return Token::I_ALPHA;
}

} // namespace <anonymous>

void case_categoriser(Suite& suite)
{
	std::istringstream iss("def func();");
	ITokenStream its(&iss);

	its.setCategoriserAlpha(&categoriserAlpha);

	Token t;
	its >> t;
	suite.assertEqual(t.category(), K_DEF, "Cat 1");
	suite.assertEqual(t.content(), "def", "Content 1");
	its >> t;
	suite.assertEqual(t.category(), Token::I_ALPHA, "Cat 2");
	suite.assertEqual(t.content(), "func", "Content 2");
}
