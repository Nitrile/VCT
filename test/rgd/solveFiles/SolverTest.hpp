#pragma once

#include <unordered_set>

#include <VCT/rgd/Solver.hpp>


namespace vct::rgd
{

class SolverTest final: public Solver
{
public:
	virtual bool
	solveFile(Resource const&, NodeProvider* const) override;

	virtual bool
	solveNode(Node const&, NodeProvider* const) override { return false; }

	virtual bool
	link(Node const&, NodeProvider* const) override { return false; }

	/// Optionally Implemented Functions ///
	
	/**
	 * Returns a regex that filters the input files, say by file extension.
	 */
	virtual std::regex filter() const override
	{ return std::regex("\\.cpp$|\\.hpp$"); }

	int nExec() const { return nExec_; }
	bool verifyHit() const;

private:
	int nExec_ = 0;

	std::unordered_set<std::string> hit_;
};

} // namespace vct::rgd
