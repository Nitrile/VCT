#include "SolverTest.hpp"

#include <iostream>

#include <VCT/rgd/Library.hpp>

namespace vct::rgd
{

bool SolverTest::solveFile(Resource const& r, NodeProvider* const)
{
	++nExec_;
	std::cout << static_cast<std::string>(r) << '\n';
	hit_.insert(static_cast<std::string>(r));
	return true;
}

bool SolverTest::verifyHit() const
{
	std::unordered_set<std::string> const target{
		"Test:SolverTest.hpp",
		"Test:SolverTest.cpp",
		"Test:main.cpp",
	};

	return target == hit_;
}

} // namespace vct::rgd
