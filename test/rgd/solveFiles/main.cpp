#include <common/Suite.hpp>

#include <VCT/rgd/System.hpp>
#include <VCT/rgd/Library.hpp>

#include "SolverTest.hpp"


int main(int argc, char* argv[])
{
	using namespace vct::rgd;

	if (argc != 2)
	{
		std::cerr << "Need two arguments, with the second being test/rgd/System\n";
		return -1;
	}

	Suite suite;

	SolverTest st;

	System system;

	system.setCacheDirectory("/tmp");

	std::filesystem::path pathBase(argv[1]);
	system.setTarget("Test", pathBase);
	system.solveFiles(&st);
	suite.assert(st.verifyHit(), "Hit target different from expected!");

	return suite.accumulate();
}
