
if (CMAKE_BUILD_TYPE MATCHES Release)
	option(opt_IndividualLibrary
	       "Build dynamic libraries for each sublibrary"
	       OFF)
else ()
	option(opt_IndividualLibrary
	       "Build dynamic libraries for each sublibrary"
	       ON)
endif ()

