include(CMakeParseArguments)

macro(EnsureNonempty ${TARGET})
	if (TARGET)
	elseif()
		message(FATAL_ERROR Target is empty: ${TARGET})
	endif()
endmacro()

function(Component)
	set(arg_oneval TARGET)
	set(arg_multival HEADERS SOURCES DEPENDS)
	cmake_parse_arguments(Co
		""
		"${arg_oneval}"
		"${arg_multival}"
		${ARGN}
	)

	set(ObjName "${Co_TARGET}.obj")
	add_library(${ObjName} OBJECT ${Co_SOURCES})
	if (${opt_IndividualLibrary})
		add_library("${Co_TARGET}"        SHARED $<TARGET_OBJECTS:${ObjName}>)
		add_library("${Co_TARGET}.static" STATIC $<TARGET_OBJECTS:${ObjName}>)
		set_target_properties(${Co_TARGET} PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES ${INCLUDE_OUTPUT_PATH}
			)


		target_link_libraries("${Co_TARGET}" PUBLIC stdc++fs)
	endif ()

	foreach(header ${Co_HEADERS})
		configure_file(${header}
			${INCLUDE_BASE_PATH}/${Co_TARGET}/${header}
			@ONLY)
	endforeach()

	#export(TARGETS ${Co_TARGET}
	#	FILE "${CMAKE_INSTALL_PREFIX}/${Co_TARGET}Config.cmake")
	#include (CMakePackageConfigHelpers)
	#write_basic_package_version_file(
	#	"${CMAKE_INSTALL_PREFIX}/${Co_TARGET}ConfigVersion.cmake"
	#	VERSION ${PROJECT_VERSION}
	#	COMPATIBILITY SameMajorVersion
	#	)
endfunction()

function(TestExecutable)
	set(arg_oneval TARGET)
	set(arg_multival HEADERS SOURCES ARGUMENTS DEPENDS)
	cmake_parse_arguments(T
		""
		"${arg_oneval}"
		"${arg_multival}"
		${ARGN}
	)

	add_executable(${T_TARGET} ${T_SOURCES})
	if (${opt_IndividualLibrary})
		target_link_libraries(${T_TARGET} ${T_DEPENDS})
	else ()
		target_link_libraries(${T_TARGET} VCT)
	endif ()
	set_target_properties(${T_TARGET}
		PROPERTIES
		RUNTIME_OUTPUT_DIRECTORY ${TEST_OUTPUT_PATH}
	)
	add_test(NAME ${T_TARGET}
		COMMAND ${TEST_OUTPUT_PATH}/${T_TARGET} ${T_ARGUMENTS})
endfunction()

