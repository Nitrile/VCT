#ifndef VCT_rgd_System_hpp_
#define VCT_rgd_System_hpp_

#include <filesystem>
#include <memory>

#include "auxiliary/diagnosis.hpp"

namespace vct::rgd
{

class Library;
class Solver;

/**
 * A tree is a logical directory. It should be mapped to a physical directory.
 */
class System final
{
public:
	System();
	~System();

	/**
	 * Non-copyable class
	 * @{ 
	 */
	System(System const&) = delete;
	System(System&&) = delete;
	System& operator=(System const&) = delete;
	System& operator=(System&&) = delete;
	/**
	 * @}
	 */

	/**
	 * System pre-processing functions. Must call these functions before using
	 * solve()
	 * @{ Set the target build library
	 */
	void setTarget(std::string const& name, std::filesystem::path const& p)
	{ setTarget(std::make_unique<Library>(name, p, false)); }
	void setTarget(std::unique_ptr<Library>&& target);
	void setCacheDirectory(std::filesystem::path&&);
	/**
	 * @}
	 */
	void setVerbosity(Verbosity const v) { verbosity_ = v; }
	void setConcurrency(int const n) { nThreads = n; }

	bool solveFiles(Solver* const);

private:
	std::filesystem::path pathCache_; ///< Stores build cache files

	std::unique_ptr<Library> target_; ///< Target library to be resolved

	Verbosity verbosity_;
	int nThreads = 0; ///< Number of threads to spawn
};

} // namespace vct::rgd

#endif // !VCT_rgd_System_hpp_
