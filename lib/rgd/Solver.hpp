#ifndef VCT_rgd_Solver_hpp_
#define VCT_rgd_Solver_hpp_

#include <regex>

namespace vct::rgd
{

class Resource;
class Node;
class NodeProvider;

class Solver
{
public:
	virtual ~Solver() = default;

	virtual bool
	solveFile(Resource const&, NodeProvider* const) = 0;

	virtual bool
	solveNode(Node const&, NodeProvider* const) = 0;

	virtual bool
	link(Node const&, NodeProvider* const) = 0;

	/// Optionally Implemented Functions ///
	
	/**
	 * Returns a regex that filters the input files, say by file extension.
	 */
	virtual std::regex filter() const { return std::regex(); }
};

} // namespace vct::rgd

#endif // !VCT_rgd_Solver_hpp_
