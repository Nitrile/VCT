#ifndef VCT_rgd_Library_hpp_
#define VCT_rgd_Library_hpp_

#include <filesystem>
#include <iostream>
#include <memory>
#include <string>

namespace vct::rgd
{

class Library;

/**
 * Represents the path to a source file.
 */
class Resource final
{
public:
	Resource(Library* const lib, std::filesystem::path const& p)
		: lib_(lib), path_(p) {}

	std::unique_ptr<std::istream> read() const;

	operator std::string() const;

private:
	Library* lib_;
	std::filesystem::path path_;
};

class Library final
{
public:
	Library(std::string const& name, std::filesystem::path const& root,
	        bool external = true)
		: name_(name), root_(root), external_(external)
	{}

	Library(Library const&) = delete;
	Library(Library&&) = delete;
	Library& operator=(Library const&) = delete;
	Library& operator=(Library&&) = delete;

	std::string const& name() const { return name_; }
	std::filesystem::path const& root() const { return root_; }

	/**
	 * If external is true, nodes from this library are read only.
	 */
	bool external() const { return external_; }

	/**
	 * Attempt to read the file.
	 *
	 * May throw vct::rgd::ExecptionFileInaccessible
	 */
	std::unique_ptr<std::istream> readPath(std::filesystem::path const&) const;

private:
	std::string name_;
	std::filesystem::path root_;

	bool external_;
};

/// Implementations ///

inline std::unique_ptr<std::istream>
Resource::read() const { return lib_->readPath(path_); }
inline Resource::operator std::string() const
{ return lib_->name() + ':' + path_.string(); }


} // namespace vct::rgd

#endif // !VCT_rgd_Library_hpp_
