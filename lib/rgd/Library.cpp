#include "Library.hpp"

#include <fstream>

#include "auxiliary/exceptions.hpp"

namespace vct::rgd
{

std::unique_ptr<std::istream>
Library::readPath(std::filesystem::path const& p) const
{
	std::ifstream* const ifs = new std::ifstream(root_ / p);
	if (ifs->is_open())
		return std::unique_ptr<std::istream>(ifs);
	else
		throw ExceptionFileInaccessible((root_ / p).string());
}



} // namespace vct::rgd

