#ifndef VCT_rgd_auxiliary_output_hpp_
#define VCT_rgd_auxiliary_output_hpp_

#include <iostream>

/// RGD Internal header ///

namespace vct::rgd
{
	std::ostream& rgd_cerr()
	{
		return std::cerr << "[RGD] ";
	}


} // namespace vct::rgd

#endif // !VCT_rgd_auxiliary_output_hpp_
