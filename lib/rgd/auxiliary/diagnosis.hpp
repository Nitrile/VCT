#ifndef VCT_rgd_auxiliary_diagnosis_hpp_
#define VCT_rgd_auxiliary_diagnosis_hpp_

namespace vct::rgd
{

enum class Verbosity: char
{
	Silent,
	Normal,
	Paranoid
};

inline bool operator< (Verbosity x, Verbosity y) { return x <  y; }
inline bool operator> (Verbosity x, Verbosity y) { return x >  y; }
inline bool operator<=(Verbosity x, Verbosity y) { return x <= y; }
inline bool operator>=(Verbosity x, Verbosity y) { return x >= y; }

} // namespace vct::rgd

#endif // !VCT_rgd_auxiliary_diagnosis_hpp_
