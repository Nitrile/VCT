#ifndef VCT_rgd_auxiliary_exceptions_hpp_
#define VCT_rgd_auxiliary_exceptions_hpp_

#include <stdexcept>

namespace vct::rgd
{

/**
 * Indicate that a file which is supposed to accessible, is not.
 */
class ExceptionFileInaccessible final: public std::runtime_error
{
public:
	ExceptionFileInaccessible(std::string const& msg)
		: std::runtime_error(msg) {}
};

/**
 * Indicate that a cache file has been corrupted.
 */
class ExceptionCacheCorruption final: public std::runtime_error
{
public:
	ExceptionCacheCorruption(std::string const& msg)
		: std::runtime_error(msg) {}
};

/**
 * Indicates that write-only
 */
class ExceptionInvalidWrite final: public std::runtime_error
{
public:
	ExceptionInvalidWrite(std::string const& msg)
		: std::runtime_error(msg) {}
};

class ExceptionSystem final: public std::runtime_error
{
public:
	ExceptionSystem(std::string const& msg)
		: std::runtime_error(msg) {}
};

} // namespace vct::rgd

#endif // ! VCT_rgd_auxiliary_exceptions_hpp_
