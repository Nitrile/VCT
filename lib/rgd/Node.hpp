#ifndef VCT_rgd_Node_hpp_
#define VCT_rgd_Node_hpp_

#include <unordered_set>

namespace vct::rgd
{

class Node
{
public:
	virtual ~Node() = default;

	Node(Node const&) = delete;
	Node(Node&&) = delete;
	Node& operator=(Node const&) = delete;
	Node& operator=(Node&&) = delete;

protected:
	Node(std::string const& name)
		: name_(name): parent_(nullptr) {}
	Node(std::string const& name, Node* const& parent)
		: name_(name), parent_(parent)
	{
		parent->children_->insert(this);
	}

private:
	std::string name_;
	Node* parent_;
	std::unordered_set<Node*> children_;

};

/**
 * A "namespace"
 */
class NodeSpace final: public Node
{
public:
	using Node::Node;

};

} // namespace vct::rgd

#endif // !VCT_rgd_Node_hpp_
