#include "System.hpp"

#include "Library.hpp"
#include "Solver.hpp"
#include "auxiliary/exceptions.hpp"
#include "auxiliary/output.hpp"

namespace vct::rgd
{

System::System()
	:
#ifndef NDEBUG
		verbosity_(Verbosity::Silent)
#else
		verbosity_(Verbosity::Paranoid)
#endif
{
}
System::~System()
{
}

void System::setTarget(std::unique_ptr<Library>&& target)
{
	if (!target || target->external())
		throw ExceptionSystem("Invalid library set as target!");

	target_ = std::move(target);
}
void System::setCacheDirectory(std::filesystem::path&& p)
{
	if (p.empty())
		throw ExceptionSystem("Empty cache directory");

	pathCache_ = std::move(p);
}

bool System::solveFiles(Solver* const solver)
{
	// Check all conditions are met
	if (!solver)
	{
		throw ExceptionSystem("Solver is null\n");
	}
	if (!target_)
	{
		if (verbosity_ >= Verbosity::Normal)
			rgd_cerr() << "System has no target. Exit.\n";

		return true;
	}
	if (pathCache_.empty())
	{
		throw ExceptionSystem("No cache directory was set.\n");
	}

	auto const sourceFilter = solver->filter();

	// Resolve all files in target library
	
	namespace stdfs = std::filesystem;

	for (auto const& entry:
			stdfs::recursive_directory_iterator(target_->root()))
	{
		// Skip non-regular files
		if (!entry.is_regular_file())
			continue;

		stdfs::path const& path = entry.path();

		// Filter through the source filter
		if (!std::regex_search((std::string) path.filename(), sourceFilter))
			continue;

		std::filesystem::path const pathRelative =
			stdfs::relative(path, target_->root());

		Resource const resource(target_.get(), pathRelative);

		bool const flag = solver->solveFile(resource, nullptr);
		if (!flag)
		{
			if (verbosity_ >= Verbosity::Normal)
				rgd_cerr() << "Solving symbol failed: "
				           << (std::string) resource
				           << std::endl;
			return false;
		}
	}
	
	return true;
}

} // namespace vct::rgd
