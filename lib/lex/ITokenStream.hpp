#ifndef __VCT_lex_ITokenStream_hpp_
#define __VCT_lex_ITokenStream_hpp_

#include <istream>

#include "Token.hpp"

namespace vct::lex
{

class ITokenStream final
{
public:
	ITokenStream(std::istream* const is);

	void resetCursor()
	{
		resetCursor(1, 0);
	}
	void resetCursor(int row, int col)
	{
		row_ = row;
		col_ = col;
	}

	ITokenStream&
	operator>>(Token& t);

	bool isEof() const { return is_->eof(); }

	void setCategoriserAlpha(Token::Categoriser cat)
	{ catAlpha_ = cat; }
	void setCategoriserSymbol(Token::Categoriser cat)
	{ catSymbol_ = cat; }
private:
	int nextCh();

	Token::Categoriser catAlpha_;
	Token::Categoriser catSymbol_;
	std::istream* is_;
	int row_, col_;

	std::string buffer_;
};

} // namespace vct::lex

#endif // !__VCT_lex_ITokenStream_hpp_
