#ifndef __VCT_lex_char_hpp_
#define __VCT_lex_char_hpp_

namespace vct::lex
{

inline bool
isSpace(char const c)
{
	return c == ' ' || c == '\t' || c == '\n';
}
inline bool
isNumeric(char const c)
{
	return c <= '9' && c >= '0';
}
inline bool
isAlpha(char const c)
{
	return ('a' <= c && c <= 'z') ||
	       ('A' <= c && c <= 'Z') ||
	       c == '_' ||

	       // To maintain compatibility with UTF-8, upper (128 <= c)
	       // characters are considered alphabets.
	       (unsigned char)(c) > 127;
}
/**
 * Is c alphanumeric?
 */
inline bool
isAlnum(char const c)
{
	return isAlpha(c) || isNumeric(c);
}
/**
 * Is c a symbol (other than the delimiters)?
 */
inline bool
isSymbol(char const c)
{
	return ('!' <= c && c <= '\'') ||
	       ('*' <= c && c <= '/') ||
	       (':' <= c && c <= '@') ||
	       c == '\\' ||
	       c == '^' ||
	       c == '`' ||
	       c == '|' || c == '~';
}

} // namespace vct::lex

#endif // !__VCT_lex_char_hpp_
