#include "ITokenStream.hpp"

#include "char.hpp"

#include <iostream>

namespace vct::lex
{

namespace
{

Token::Category
defaultCategoriserAlpha(char const* const)
{
	return Token::I_ALPHA;
}
Token::Category
defaultCategoriserSymbol(char const* const)
{
	return Token::I_SYMB;
}

} // namespace <anonymous>

ITokenStream::ITokenStream(std::istream* const is)
		: is_(is)
		, catAlpha_(&defaultCategoriserAlpha)
		, catSymbol_(&defaultCategoriserSymbol)
{ 
	resetCursor();

	constexpr std::size_t BUFFER_RESERVE = 64;
	buffer_.reserve(BUFFER_RESERVE);
}

ITokenStream&
ITokenStream::operator>>(Token& t)
{
	int ch;
	do
	{
		ch = nextCh();
	}
	while (ch != EOF && isSpace(ch));

	// Freeze row and column
	int const token_r = row_;
	int const token_c = col_;

	if (ch == EOF)
	{
		t = Token(std::string(), Token::END, token_r, token_c);
		return *this;
	}

	// Alphanumeric sequences
	if (isAlpha(ch))
	{
		buffer_.clear();
		buffer_ += ch;
		ch = is_->peek();
		while (ch != EOF && isAlnum(ch))
		{
			buffer_ += nextCh();
			ch = is_->peek();
		}
		t = Token(buffer_, catAlpha_(buffer_.c_str()), token_r, token_c);
		return *this;
	}
	else if (ch == '\'')
	{
		// TODO: Escape
		buffer_.clear();
		ch = nextCh();
		while (ch != EOF && ch != '\'')
		{
			buffer_ += ch;
			ch = nextCh();
		}
		if (ch == EOF)
			t = Token(buffer_, Token::INVALID, token_r, token_c);
		else
			t = Token(buffer_, Token::L_CHAR, token_r, token_c);
		return *this;
	}
	else if (ch == '\"')
	{
		// TODO: Escape
		buffer_.clear();
		ch = nextCh();
		while (ch != EOF && ch != '\"')
		{
			buffer_ += ch;
			ch = nextCh();
		}
		if (ch == EOF)
			t = Token(buffer_, Token::INVALID, token_r, token_c);
		else
			t = Token(buffer_, Token::L_STRING, token_r, token_c);
		return *this;
	}
	else if (isSymbol(ch))
	{
		buffer_.clear();
		buffer_ += ch;
		ch = is_->peek();
		while (ch != EOF && isSymbol(ch))
		{
			buffer_ += nextCh();
			ch = is_->peek();
		}
		t = Token(buffer_, catSymbol_(buffer_.c_str()), token_r, token_c);
		return *this;
	}
	else if (isNumeric(ch))
	{
		// TODO: Use a more sophisticated float detection rather than simply
		//       detecting '.'
		bool isFloat = false;
		buffer_.clear();
		buffer_ += ch;
		ch = is_->peek();
		while (ch != EOF && (isNumeric(ch) || ch == '.'))
		{
			if (ch == '.')
				isFloat = true;
			buffer_ += nextCh();
			ch = is_->peek();
		}
		if (isFloat)
			t = Token(buffer_, Token::L_FLOAT, token_r, token_c);
		else
			t = Token(buffer_, Token::L_INT, token_r, token_c);
		return *this;
	}
	else if (ch == '(')
	{
		t = Token(std::string(), Token::D_BRACKET_L, token_r, token_c);
		return *this;
	}
	else if (ch == ')')
	{
		t = Token(std::string(), Token::D_BRACKET_R, token_r, token_c);
		return *this;
	}
	else if (ch == '[')
	{
		t = Token(std::string(), Token::D_BRACKET_S_L, token_r, token_c);
		return *this;
	}
	else if (ch == ']')
	{
		t = Token(std::string(), Token::D_BRACKET_S_R, token_r, token_c);
		return *this;
	}
	else if (ch == '{')
	{
		t = Token(std::string(), Token::D_BRACKET_C_L, token_r, token_c);
		return *this;
	}
	else if (ch == '}')
	{
		t = Token(std::string(), Token::D_BRACKET_C_R, token_r, token_c);
		return *this;
	}
	else
	{
		t = Token(std::string(), Token::INVALID, token_r, token_c);
		return *this;
	}

	return *this;
}

inline int
ITokenStream::nextCh()
{
	int const ch = is_->get();

	if (ch == '\n')
	{
		++row_;
		col_ = 0;
	}
	else
	{
		++col_;
	}

	return ch;
}

} // namespace vct::lex
