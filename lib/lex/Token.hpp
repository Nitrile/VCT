#ifndef __VCT_lex_Token_hpp_
#define __VCT_lex_Token_hpp_

#include <string>

namespace vct::lex
{

/**
 * Represents a single token in the lexing stage.
 */
class Token final
{
public:
	/**
	 * Underlying integer type for Token category.
	 */
	using Category = uint8_t;
	/**
	 * Represents the token categories produced by the lexer. Users of the Token
	 * class should extend CategoryBase in the following scheme:
	 *
	 * @code{.cpp}
	 * 
	 * enum CategoryLang:
	 *   // Using the same container as CategoryBase
	 *   vct::lex::Token::Category
	 * {
	 *   // Align beginning of this category to the end of CategoryBase
	 *   K_CLASS = vct::lex::Token::CAT_MAX,
	 *   K_EXTENDS,
	 *   K_FOR,
	 *   K_WHILE,
	 *   ...
	 * };
	 *
	 * @endcode
	 */
	enum CategoryBase: Category
	{
		INVALID = 0,
		END, // EOF
		
		// Identifiers
		I_ALPHA, // Alphanumeric Identifier. e.g. xyz12
		I_SYMB,  // Symbolic identifier e.g. <#>

		// Literals
		L_INT,
		L_FLOAT,
		L_STRING, // Delimited by ""
		L_CHAR,   // Delimited by ''

		// Comments
		C_LINE, // line comment
		C_BLOCK, // block comment

		// Delimiters
		D_BRACKET_L, // Brackets () left and right
		D_BRACKET_R,
		D_BRACKET_S_L, // Square brackets [] left and right
		D_BRACKET_S_R,
		D_BRACKET_C_L, // Curly brackets {} left and right
		D_BRACKET_C_R,

		CAT_MAX
	};
	/**
	 * Function responsible for converting identifier names to more specific
	 * categories. The categoriser is only called for identifiers and not literals
	 * or comments or delimiters.
	 */
	using Categoriser = Category (*)(char const* const);


	Token() = default;
	Token(std::string const& content, Category cat, int row, int col):
		content_(content), cat_(cat), row_(row), col_(col)
	{}

	std::string const& content() const { return content_; }
	Category category() const { return cat_; }
	void setCategory(Category cat) { cat_ = cat; }

	int row() const { return row_; }
	int col() const { return col_; }

private:
	std::string content_;
	Category cat_;
	int row_, col_;
};


} // namespace vct::lex

#endif // !__VCT_lex_Token_hpp_
